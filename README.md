<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/Edenz/krayot-903-deepccc">
    <img src="images/logo.svg" alt="Logo" width="320" height="320">
  </a>

<h3 align="center">DeepCCC</h3>

  <p align="center">
    Chess, Connect-4, Creativity
    </p>
    <p align="center">
    This is an all-around Machine Learning project which uses many kinds of technologies, Including: Classification, Reinforcement Learning, 
    </p>
    <p align="center">
    <br />
    <a href="https://gitlab.com/Edenz/krayot-903-deepccc"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/Edenz/krayot-903-deepccc">View Demo</a>
    ·
    <a href="https://gitlab.com/Edenz/krayot-903-deepccc/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/Edenz/krayot-903-deepccc/-/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

<strong>DeepCCC</strong> started as a Minimax based Chess engine project but quickly became an all-around Machine Learning project with all kinds of technologies

In our Repository you will be able to find a Chess and Connect4 Minimax based engines.
In addition, we implemented a Reinforcement Learning based Connect4 engine.


We, the team members: Eden Zahko, Noam Patai would be glad to hear any suggestions or thoughts about the project :)

Contact us

* Email
  * Noam: `noam.patai@gmail.com`
  * Eden: `edenzah04@gmail.com`

* Gitlab Usernames
  * Noam: `noampatai`
  * Eden: `Edenz`

* Linkedin
  * Noam: `Noam Patai`
  * Eden: ``




<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [Python](https://www.python.org/)
* [tensorflow](https://www.tensorflow.org/)
* [Keras](https://keras.io/)
* [Keras-rl](https://github.com/keras-rl/keras-rl)
* [openAI](https://openai.com/)
* [PyQt](https://riverbankcomputing.com/software/pyqt/intro)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

In order to run the project, you will need to install a few Python libraries if you haven't already.


### Prerequisites

Our entire project is coded in python, you'll first need to install it.
* Python

  <a href="https://www.python.org/">Install the latest version of Python</a>

In addition, You'll need to have the Pip installer installed on your computer in order to install the different libraries.
* Pip

  <a href="https://www.geeksforgeeks.org/how-to-install-pip-on-windows/">Follow these instructions in order to install Pip</a>

You can now install the required packages using ```pip install```.
* Required Libraries

    * tensorflow
    * gym
    * Pillow
    * numpy
    * keras-rl2
    * PyQt5
    * chess

      These package can be installed by running ```pip install <package name>``` on your Windows command promt.
      Look online for instructions on using pip on Linux or MacOS


### Installation

Clone the repo
   ```sh
   git clone https://gitlab.com/Edenz/krayot-903-deepccc.git
   ```

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- USAGE EXAMPLES -->
## Usage

Our project has a few files that run it's parts.

* Minimax Chess engine - run `ChessUI.py`
* Minimax Connect-4 engine - run `c4_engine.py`
* Reinforcemnt Learning Connect-4 - run `Connect-4 Reinforcement Learning Model\C4ReinforcementLearningEngine.py`
* GUI - run `c4_gui_pyqt.py`


_For more examples, please refer to the [Documentation](https://example.com)_

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [ ] Comparison based Minimax Chess and Connect-4 algorithm.
    - [ ] Machine Learning model used to compare Chess and Connect-4 board positions.
- [ ] Reinforcement Learning based Connect-4 engine.
- [ ] Connect-4 board image proccessing.


See the [open issues](https://gitlab.com/Edenz/krayot-903-deepccc/-/issues?sort=created_date&state=opened) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

* This project is free to use for research and enjoyment. But, it shall not be copied, renamed, republished or taken.
* The rights for this project are only owned by the project authors, Noam Patai and Eden Zahko.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

* Noam Patai - noam.patai@gmail.com
* Eden Zahko - edenzah04@gmail.com

Project Link: [https://gitlab.com/Edenz/krayot-903-deepccc](https://gitlab.com/Edenz/krayot-903-deepccc)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

We owe a huge gratitude to the head of our team, Alex Zakashanski, for guiding us on our journey. We owe a huge gratitude to our mentor, Avishai Livne, for all of the help and support along the way.
And of course, to our friends in Maghimim for making this proccess far more interesting and enjoyable.


<p align="right">(<a href="#top">back to top</a>)</p>


